const express = require('express');
const UserController = require('../controllers/UserController.js');

const router = express.Router();

// CREATE
router.post('/', (request, response) => {
	return UserController.createUser(request, response);
});

// READ
router.get('/', (request, response) => {
	return UserController.getAllUsers(request, response);
});

// READ
router.get('/:id', (request, response) => {
	return UserController.findUser(request, response);
});

// UPDATE
router.put('/:id', (request, response) => {
	return UserController.updateUser(request, response);
});

// UPDATE
router.patch('/:id', (request, response) => {
	return UserController.updateUser(request, response);
});

// DELETE
router.delete('/:id', (request, response) => {
	return UserController.deleteUser(request, response);
});

module.exports = router;