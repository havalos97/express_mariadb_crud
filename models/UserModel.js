const Sequelize = require('sequelize');
const dbConnection = require('../settings/dbConnection.js');

const UserModel = dbConnection.define('user', {
	firstName: {
		type: Sequelize.STRING,
		allowNull: false
	},
	lastName: {
		type: Sequelize.STRING
	}
});

// ALTERNATIVE METHOD
// const Model = Sequelize.Model;
// class UserModel extends Model {}
//
// UserModel.init({
// 	firstName: {
// 		type: Sequelize.STRING,
// 		allowNull: false
// 	},
// 	lastName: {
// 		type: Sequelize.STRING
// 	}
// }, {
// 	sequelize: dbConnection,
// 	modelName: 'user',
// });

module.exports = UserModel;