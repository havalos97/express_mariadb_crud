const express = require('express');
const app = express();
const dbConnection = require('./settings/dbConnection.js');
const settings = require('./settings/settings.js');
const HOST = settings.APP_HOST;
const PORT = settings.APP_PORT;

dbConnection.authenticate().then(() => {
	const UserRoutes = require('./routes/UserRouter.js');
	console.log('Connected successfully to the database');

	app.use(express.json());
	app.use(express.urlencoded({ extended: true }));
	app.use('/user', UserRoutes);

	app.get('/', (request, response) => {
		response.status(200).json({
			message: "Hello World",
		});
	});

	app.listen(PORT, () => {
		console.log(`Server listening on http://${HOST}:${PORT}`);
	});
}).catch((error) => {
	console.error('Unable to connect to the database: ', error);
});